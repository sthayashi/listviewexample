package com.hfad.listviewexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class DynamicListActivity extends AppCompatActivity {

    List<String> dynamicList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_list);
        dynamicList.add("This is a");
        dynamicList.add("dynamic list");
        dynamicList.add("within an activity");
        ArrayAdapter<String> listAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                dynamicList
        );
        ListView dList = findViewById(R.id.dynamic_list);
        dList.setAdapter(listAdapter);
    }
}
